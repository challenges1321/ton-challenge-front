import Vue from 'vue'
import VueRouter from 'vue-router'
import CreateUser from '../views/CreateUser.vue'
import SearchUser from '../views/SearchUser.vue'
import Home from '../views/Home.vue'
import About from '../views/About.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '',
    name: 'blank',
    component: Home
  },
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/create',
    name: 'create',
    component: CreateUser
  },
  {
    path: '/search',
    name: 'search',
    component: SearchUser
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
]

const router = new VueRouter({
  routes
})

export default router
